#!/bin/sh

gnuplot <<EOT
set terminal pdfcairo

set xdata time
set timefmt '%Y-%m-%d'
set xrange ["2006-01-01":"2021-01-01"]
set xtics rotate by 45 right "2004-01-01", 31536000, "2020-12-01" center offset 0,-1 font ",8"
set ytics font ",8"
#show xlabel

set output "commits.pdf"
plot "commits.dat" using 1:2 with lines title "Commits over weeks"

set output "commiters.pdf"
plot "commiters.dat" using 1:2 with lines title "Commits over weeks"

# weekly data but ignore commiters with less than 3 commits (no big difference in the end)
set output "commiters-normalised.pdf"
plot "commiters-normalised.dat" using 1:2 with lines title "Commits over weeks"


set output "commiters-month.pdf"
set timefmt '%Y-%m'
plot "commiters-month.dat" using 1:2 with lines title "Commiters over months"

set output "commits-month.pdf"
plot "commits-month.dat" using 1:2 with lines title "Commits over months"

EOT
