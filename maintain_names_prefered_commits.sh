#!/bin/sh

psql teammetrics <<EOT
UPDATE commitstat SET name = 'Alexandre Mestiashvili' WHERE name = 'Alex Mestiashvili';
UPDATE commitstat SET name = 'Benjamin Redelings' WHERE name = 'Benjamin Redelings I';
UPDATE commitstat SET name = 'Cédric Lood' WHERE name = 'Cedric L';
UPDATE commitstat SET name = 'Cédric Lood' WHERE name = 'Cedric Lood';
UPDATE commitstat SET name = 'Cédric Lood' WHERE name = 'Milt0n';
UPDATE commitstat SET name = 'Canberk Koç' WHERE name = 'cbk' or name = 'cbk-guest';
UPDATE commitstat SET name = 'Çağrı Ulaş' WHERE name = 'Çağrı ULAŞ';
UPDATE commitstat SET name = 'Charles Plessy' WHERE name = 'charles-guest' or name = 'plessy';
UPDATE commitstat SET name = 'Christian M. Amsüss' WHERE name = 'chrysn';
UPDATE commitstat SET name = 'David Paleino' WHERE name = 'hanska-guest';
UPDATE commitstat SET name = 'Debian Janitor' WHERE name = 'Janitor';
UPDATE commitstat SET name = 'Dimitri John Ledkov' WHERE name = 'Dimitri Ledkov';
UPDATE commitstat SET name = 'Étienne Mollier' WHERE name = 'Etienne Mollier';
UPDATE commitstat SET name = 'Flavien Bridault' WHERE name = 'Flavien';
UPDATE commitstat SET name = 'Gert Wollny' WHERE name = 'gert-guest';
UPDATE commitstat SET name = 'Gregory C. Sharp' WHERE name = 'Gregory Sharp' or name = 'Greg Sharp';
UPDATE commitstat SET name = 'Hervé Ménager' WHERE name = 'Hervé Menager';
UPDATE commitstat SET name = 'Yves Martelli' WHERE name = 'ivmartel-guest';
UPDATE commitstat SET name = 'James McCoy' WHERE name = 'jamessan';
UPDATE commitstat SET name = 'Jaakko Leppakangas' WHERE name = 'jaeilepp';
UPDATE commitstat SET name = 'Jelmer Vernooĳ' WHERE name = 'Jelmer Vernooij';
UPDATE commitstat SET name = 'Julien Y. Dutheil' WHERE name = 'Julien Dutheil';
UPDATE commitstat SET name = 'Karolis Kalantojus' WHERE name = 'Karolis';
UPDATE commitstat SET name = 'Katerina Kalou' WHERE name = 'kkalou-guest' or name = 'Kalou';
UPDATE commitstat SET name = 'Mathieu Malaterre' WHERE name = 'malat-guest' or name = 'malat';
UPDATE commitstat SET name = 'Matúš Kalaš' WHERE name = 'matuskalas';
UPDATE commitstat SET name = 'Michael Hanke' WHERE name = 'mhanke-guest';
UPDATE commitstat SET name = 'Michael Schutte' WHERE name = 'michi';
UPDATE commitstat SET name = 'Gianfranco Costamagna' WHERE name like 'locutusofborg%';
UPDATE commitstat SET name = 'Nadiya Sitdykova' WHERE name = 'nadiya' or name = 'Nadiya Sitdykov' or name = 'rovenskasa-guest';
UPDATE commitstat SET name = 'Nelson A. de Oliveira' WHERE name like 'naoliv';
UPDATE commitstat SET name = 'Paul Novotny' WHERE name = 'paulnovo-guest';
UPDATE commitstat SET name = 'Rafael Laboissière' WHERE name = 'Rafael Laboissiere';
UPDATE commitstat SET name = 'Alexandre Mestiashvili' WHERE commit_id = '82262500d6fa30d5596111e50eb778595649278d';
UPDATE commitstat SET name = 'Olivier Sallou' WHERE email='root@601410a29ab0' or email='root@79824077b522';
UPDATE commitstat SET name = 'Michael R. Crusoe' WHERE commit_id = '60cc2ad06b09f9f9aa320b2731c364019e5e7286';
UPDATE commitstat SET name = 'Yaroslav Halchenko' WHERE commit_id = '4c9de6817abcf85b39c3777dd4705cc20c484a79';
UPDATE commitstat SET name = 'Sao I Kuan' WHERE name = 'Sao-I Kuan';
UPDATE commitstat SET name = 'Sébastien Jodogne' WHERE name = 'jodogne-guest';
UPDATE commitstat SET name = 'Sébastien Jodogne' WHERE name = 'Sebastien Jodogne';
UPDATE commitstat SET name = 'Shamika Mohanan' WHERE name = 'shamikam';
UPDATE commitstat SET name = 'Shayan Doust' WHERE name = 'shayan_doust';
UPDATE commitstat SET name = 'Steffen Möller' WHERE name = 'Steffen Moeller';
UPDATE commitstat SET name = 'Thiago Franco de Moraes' WHERE name = 'Thiago Franco Moraes';
UPDATE commitstat SET name = 'Timothy Booth' WHERE name = 'Tim Booth';
UPDATE commitstat SET name = 'Andreas Tille' WHERE name = 'tille';
UPDATE commitstat SET name = 'Tony Mancill' WHERE name = 'tony mancill';
UPDATE commitstat SET name = 'Yaroslav Halchenko' WHERE name = 'yoh-guest';
UPDATE commitstat SET name = 'Zhao Feng' WHERE name = 'zhaofeng';
UPDATE commitstat SET name = 'Zhao Feng' WHERE name = 'zhaofeng-shu33';
UPDATE commitstat SET name = 'Afif Elghraoui' WHERE name = 'عفيف الغراوي';
UPDATE commitstat SET name = 'Xiao Sheng Wen' WHERE name = '肖盛文';

EOT
